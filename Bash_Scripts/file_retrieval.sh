#!/bin/bash

# Exit on error
set -o errexit

# Exit on errir inside any functions or subshells
set -o errtrace

# Do not allow undefined vars. Use ${VAR:-} to use an undefined var
set -o nounset

# Don't hide errors in pipes
set -o pipefail

# Traces for debugging
#set -o xtrace

trap remove_tmp_files SIGINT SIGTERM EXIT

configure_variables() {
    timestamp="$(date '+%Y-%m-%d')"
    logs_dir="$HOME/.logs"
    config_dir="$HOME/.config/"
    config_file="${config_dir}config.json"
    script_log="${HOME}/.logs/file_retrieval_${timestamp}.log"
    clamav_results_file="${HOME}/.logs/file_scan_results_${timestamp}.log"
    smb_credentials="$HOME/.config/smb_credentials"
}

remove_tmp_files() {
    if [[ -d "${logs_dir}" ]]; then
        if [[ -f "${logs_dir}/running_file" ]]; then
             rm "${logs_dir}/running_file" 
        fi

        if [[ -f "${logs_dir}/smb_hash" ]]; then
             rm "${logs_dir}/smb_hash" 
        fi

        if [[ -f "${logs_dir}/odin_hash" ]]; then
             rm "${logs_dir}/odin_hash" 
        fi
    fi
}

env_check() {
    if [[ ! -d "${HOME}/.logs" ]]; then
        mkdir -p "${HOME}/.logs"
    else
        remove_tmp_files
    fi

    if [[ -f "${logs_dir}/running_file" ]]; then
        echo "[-] This Script $0 is already running" >> "${script_log}"
        echo "[!] Exiting..." >> "${script_log}"
        exit 1
    fi

    if [[ ! -f "${smb_credentials}" ]]; then
        echo "[-] The Samba Credentials does not exist in ${config_dir}" >> "${script_log}"
        echo "[!] Exiting..." >> "${script_log}"
        exit 1
    fi

    if [[ ! -f "${config_file}" ]]; then
        echo "[-] Config file does not exist in ${config_dir} :(" >> "${script_log}"
        echo "[!] Exiting Script..." >> "${script_log}"
        exit 1
    fi

    smb_dir="$(cat $config_file  | jq -r .smb_source_dir | openssl enc -aes-256-cbc -md sha512 -a -d -pbkdf2 -iter 100000 -salt -pass pass:A341.bw2x )"
    odin_dir="$(cat $config_file  | jq -r .odin_dir | openssl enc -aes-256-cbc -md sha512 -a -d -pbkdf2 -iter 100000 -salt -pass pass:A341.bw2x )"

    mountpoint $smb_dir >> "${script_log}"
    smb_mounted=$?

    if [[ $smb_mounted != 0 ]];then
        echo "[-] The Samba drive is not mounted at ${smb_dir}" >> "${script_log}"
        echo "[!] Exiting..." >> "${script_log}"
        exit 1
    fi


    if [[ ! -d $odin_dir ]]; then
        mkdir -p  $odin_dir
    fi

    touch "${logs_dir}/running_file"

}

get_smb_dir_changes() {
    retries=3
    hashes_match=0

    while [ $retries -gt 0 ]
    do
        if [ $retries -gt 0 ]; then
            retries=$((retries - 1))
        fi
        echo "[!] Starting Samba File retrieval script." >> "${script_log}"
        rsync -av $smb_dir $odin_dir >>  "${script_log}" 2>&1

        echo >> "${script_log}"
        echo "[!] Creating SMB Hash." >> "${script_log}"
        find "${smb_dir}" -type f -exec sha256sum {} \;  | awk {'print $1'} | sort -n | sha256sum | awk {'print $1'} > "${logs_dir}/smb_hash" &

        echo >> "${script_log}"
        echo "[!] Creating Odin Hash." >> "${script_log}"
        find "${odin_dir}" -type f -exec sha256sum {} \; | awk {'print $1'} | sort -n | sha256sum | awk {'print $1'} > "${logs_dir}/odin_hash" &
        wait
        samba_hash=$(cat "$logs_dir/smb_hash")
        odin_hash=$(cat "$logs_dir/odin_hash")
        echo >> "${script_log}"
        echo "[!] Samba Directory Hash: ${samba_hash}" >> "${script_log}"
        echo "[!] Odin Directory Hash: ${odin_hash}" >> "${script_log}"
        if [ ! "${samba_hash}" = "${odin_hash}" ]; then
             echo "[-] Source and destination directories do not match!" >> "${script_log}"
             echo "[!] Retrying Copy and rehashing" >> "${script_log}"
        else
            echo "[+] Source and destination directories match" >> "${script_log}"
            hashes_match=1
            break
        fi
    done

    if [[ $retries < 1 ]] && [[ $hashes_match != 1 ]]; then
        echo "[-] Hashes do not match and retried 3 times." >> "${script_log}"
        echo "[!] Exiting Script..." >> "${script_log}"
        exit 1
    fi

    rm "${logs_dir}/smb_hash" "${logs_dir}/odin_hash"
}

main() {
    # Set inital env variables.
    configure_variables

    # Check that all the required directories and files that should exist, exist and the temp files are removed.
    env_check
    
    # Pull the files via rsync to the odin directory.
    get_smb_dir_changes

    # Run clamscan on the odin directory and output to log file.
    echo "[!] Starting ClamAV scan on the Odin log files." >> "${script_log}"
    echo "[!] Please check ${clamav_results_file} to the results of the scan." >> "${script_log}"
    clamscan -r "${odin_dir}" -l "${clamav_results_file}" >> "${script_log}" 2>&1


    # Output script details and give exit code on completion.
    script_name1="basename ${0}"
    script_path1="$(dirname "$(readlink -f "${0}")")"
    script_path_with_name="${script_path1}/${script_name1}"
    echo "Script path with name: ${script_path_with_name}" >> "${script_log}"
    echo "Script finished" >> "${script_log}"
    exit 0
}

main
