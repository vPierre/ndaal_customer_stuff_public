from influxdb_client import InfluxDBClient, Point, WriteOptions
from influxdb_client.client.write_api import SYNCHRONOUS
import pandas as pd
from influxdb_client.client.write_api import ASYNCHRONOUS
from datetime import timedelta

bucket = "telegraf"

client = InfluxDBClient(url="http://localhost:8086", token="my-token", org="my-org")

write_api = client.write_api(write_options=WriteOptions(batch_size=5000, flush_interval=10_000, jitter_interval=2_000, retry_interval=5_000))
#query_api = client.query_api()


#write_api = client.write_api()

#p = Point("my_measurement").tag("location", "Prague").field("temperature", 25.3)

## Dataframe ###
Data = pd.read_csv("UNSW_NB15_training-set.csv") ## change to name of your data

#print(Data)

print(pd.to_datetime('now'))
Df=Data.insert(0, 'TimeStamp', pd.to_datetime('now'))
#print(type(Df))
#print(Df.column)
#write_api.write(bucket=bucket, record=p)

write_api.write("telegraf", "my-org", record=Df, data_frame_measurement_name="anamoly")


## using Table structure
#tables = query_api.query('from(bucket:"telegraf") |> range(start: -10m)')
#
#for table in tables:
#    print(table)
#    for row in table.records:
#        print (row.values)
#
#
### using csv library
#csv_result = query_api.query_csv('from(bucket:"telegraf") |> range(start: -10m)')
#val_count = 0
#for row in csv_result:
#    for cell in row:
#        val_count += 1