## Influxdb_client ## 

This Repo contains a basic example code for writing and querying DataFrame to InfluxDB using [InfluxDb_client](https://github.com/influxdata/influxdb-client-python","InfluxDb_client) package in Python.

The file 'requirements.txt' contains the required libraries to run.

At first, we need to make an instance of InfluxDBclient to make a connection with the InfluxDB. For connection, it needs identity details of the clients so that a client can be configured via *.ini file in segment influx2.

The influx2 segment of "config.ini" contains:

- url - the url to connect to InfluxDB (Please change the ip to your Influx machine Ip)
- org - default destination organization for writes and queries
- token - the token to use for the authorization
- timeout - socket timeout in ms (default value is 10000)
- verify_ssl - set this to false to skip verifying SSL certificate when calling API from https server

Then with the help of 'InfluxDBClient.from_config_file()' we setup the client.

### WriteApi
After setting up the client, we need to write data to the InfluxDB client. For that, at first, we read data as a Dataframe using [Pandas](https://pandas.pydata.org/pandas-docs/stable/, "Pandas"), then uses the ["WriteApi"](https://github.com/influxdata/influxdb-client-python/blob/025e97e4d78a5fd0085344b4da1e6cd7314fd26b/influxdb_client/client/write_api.py,"write_api") class to write the DataFrame to the InfluxDb.

The WriteApi supports synchronous, asynchronous and batching writes into InfluxDB 2.0. The data could be written as

1.string or bytes that is formatted as a InfluxDB's line protocol 

2.Data Point structure

3.Dictionary style mapping with keys: measurement, tags, fields and time

4.List of above items

5.A batching type of write also supports an Observable that produce one of an above item

6.Pandas DataFrame

Here we need to write data as a DataFrame object. The default instance of WriteApi use batching and the batching is configurable by 'write_options'. The Batching options are:
- batch_size : The number of data pointx to collect in a batch
- flush_interval : The number of milliseconds before the batch is written
- jitter_interval : The number of milliseconds to increase the batch flush interval by a random amount
- retry_interval : The number of milliseconds to retry first unsuccessful write. The next retry delay is computed using         exponential random backoff. The retry interval is used when the InfluxDB server does not specify "Retry-After" header
- max_retry_time : Maximum total retry timeout in milliseconds
- max_retries : The number of max retries when write fails
- max_retry_delay : the maximum delay between each retry attempt in milliseconds
- exponential_base :the base for the exponential retry delay, the next delay is computed using random exponential backoff as a random value within the interval retry_interval * exponential_base^(attempts-1) and retry_interval * exponential_base^(attempts). Example for retry_interval=5_000, exponential_base=2, max_retry_delay=125_000, total=5 Retry delays are random distributed values within the ranges of [5_000-10_000, 10_000-20_000, 20_000-40_000, 40_000-80_000, 80_000-125_000]

Then writes data to influx using 'write' function of WriteApi with parameters of:
- Organization org: specifies the destination organization for writes;
                  take the ID, Name or Organization;
                  if it's not specified then is used default values.
- bucket: specifies the destination bucket for writes (required)
- WritePrecision write_precision: specifies the precision for the unix timestamps within the body line-protocol. The precision specified on a Point has preceded and is used for write.
- record: Points, line protocol, Pandas DataFrame, RxPY Observable to write
- data_frame_measurement_name: name of measurement for writing Pandas DataFrame
 - data_frame_tag_columns: list of DataFrame columns which are tags, rest columns will be fields

### QueryApi
Once the writing to influxdb is successful, we will try to query this data as a Dataframe from Influxdb.

The ["QueryApi"](https://github.com/influxdata/influxdb-client-python/blob/cf2186200156c1f5cc2008703bb54775241d2aba/influxdb_client/client/query_api.py#L34,"QueryAp") helps to query data from influx.
The result retrieved by QueryApi could be formatted as :

1.Flux data structure: FluxTable, FluxColumn and FluxRecord

2.Query bind parameters

3.csv.reader which will iterate over CSV lines

4.Raw unprocessed results as a str iterator
5.Pandas DataFrame

For querying as DataFrame, we need to  install Pandas dependency via "pip install 'influxdb-client[extra]'".

The client can retrieve data in Pandas DataFrame format through query_data_frame. query_data_frame takes parameters of :

- query: the Flux query
- Organization org: specifies the organization for executing the query;
                    take the ID, Name or Organization;
                    if it's not specified, then is used default
- data_frame_index: the list of columns that are used as DataFrame index
- params: bind parameters

With query parameter, we fetch the data using ["Flux"](https://docs.influxdata.com/influxdb/v2.0/query-data/get-started/) query language.

In the end, we need to close the client connection with  client.close()
