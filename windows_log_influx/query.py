from influxdb_client import InfluxDBClient, Point, Dialect
from influxdb_client.client.write_api import SYNCHRONOUS
import datetime
import os
import pandas as pd
import csv
import rsa

token = b'R\x15\xf8\xd3&!d\xd5\xfa\nT\xe2\xfa\xca\xee\xfe\xf2\x07y!\'?+Ng\xef\xbd\x85$\xdc7\xae\x1c\xae|\x05~U\xfaf\xf43;k\x06\x0b^\xa1\x90r\x1fIuO\xa43\x84n\xbb\xadK\x88\x1a\xe5\xd1"\x8b\xcf\x9an\xc5\x0f\xe7\xc2\'\xe8\x12\xc4\x88\xf0q\x8d\xba\xa1\xd2\xd7\xb0\xbb\xa0\xd7\x9d\x17\xa7\xd8R\x87\x83\xe47\xa3\xbe\xe7\xed?A9\x93\xf9h\x05!\x83\xfb`H\xc1\x82\xb2H\x84m:\xcb\xc4b^\xc4\xec>\xf3Q\xb9)d\xefMogZ\x9ef\xbd\xdeH\xeb\xcbk\x16BV\xd9\xf2\xc5B\xc9TJ\x0b\x8f\xc3\xfc\xc3;]NX.\xb8&\xaak\\Prj\xcf\x83\x84\xfc\x947\xcc\x00\xabfY\xf3\xf8\x90$\xc1\xe5B[\xef\xa0\x9e\x0c\x01\xa9\xa3[\xe3@\xf6O\x92\xf3\x93\x16\xdd\xc9\xb2xE\x02 \xdbH\xa6\xdf\x93\xfa}v%j\xf7(`s\xb1\xc4\xafAswP1]\xb2X\xf8\xe5\x06\xee7r\xe6\xac\xf7\xe8\x06\xf1\xcbF'
with open('private_key.pem','rb') as privatefile:
    keydata = privatefile.read()
privkey = rsa.PrivateKey.load_pkcs1(keydata,'PEM')

influx_token = rsa.decrypt(token, privkey).decode()

client = InfluxDBClient(url="https://ec2-18-192-99-56.eu-central-1.compute.amazonaws.com:8086", token=influx_token, org="ndaal",verify_ssl=False)
#client = InfluxDBClient.from_config_file("config.ini")

query_api = client.query_api()

csv_result = query_api.query_csv('from(bucket:"test") |> range(start: -6h)|> filter(fn: (r) => r["_measurement"] == "win_eventlog")',
                              dialect=Dialect(header=True, delimiter=",", comment_prefix="#", annotations=[],
                              date_time_format="RFC3339"))
list_csv = []
for csv_line in csv_result:
      list_csv.append(csv_line)

columns = ['', 'result', 'table', '_start', '_stop', '_time', '_value', 'Channel', 'Computer', 'EventID', 'Keywords', 'Level', 'LevelText', 'Opcode', 'OpcodeText', 'Source', 'Task', 'UserName', '_field', '_measurement', 'host']
rows = list_csv
with open('log.csv', 'w') as f:
      write = csv.writer(f)
      write.writerow(columns)
      write.writerows(rows)
log_df = pd.read_csv("log.csv", low_memory=False)
df = log_df[['_time', '_value', 'Channel', 'Computer', 'EventID', 'Keywords', 'Level', 'LevelText', 'Opcode', 'OpcodeText', 'Source', 'Task', 'UserName', '_field', '_measurement', 'host']]
df = df[1:]
dir = "Logs"
if not os.path.isdir(dir):
      os.makedirs(dir)
print(df['Computer'].unique())
for host in df['Computer'].unique():
      if host !="Computer":
            with open(os.path.join(dir,str(host)+".log"), "w") as my_output_file:
                  df_1 = df[df['Computer']==host]
                  row_list = df_1.values.tolist()
                  for lists in row_list:
                        if len(lists) != 0:
                              text_list =  ",".join(list(map(str, lists)))
                              my_output_file.write(text_list+"\n")
                  my_output_file.close()

