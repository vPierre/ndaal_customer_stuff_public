import rsa


# this is the string that we will be encrypting
message = "UeVd9RK46yb4rxbWr_xwDqY8DJXYEhrlzYBE-nDmrPSH_1FNJApL5B4yjxtFOFHeTWG5gimjY8hGN9gC5awpEw=="

with open('token.txt') as key:
            token = key.read()
print(token)
with open('private_key.pem','rb') as privatefile:
    keydata = privatefile.read()
privkey = rsa.PrivateKey.load_pkcs1(keydata,'PEM')

with open('public_key.pem','rb') as publicfile:
    publicKey = publicfile.read()
    public_key = rsa.PublicKey.load_pkcs1_openssl_pem(publicKey)
#openssl rsa -in key.pem -RSAPublicKey_out -out pubkey.pempubkey = rsa.PublicKey.load_pkcs1(publicKey)
#print(privkey)
#print(public_key)


encMessage = rsa.encrypt(token.encode(),
                         public_key)

print(encMessage)

#decMessage = rsa.decrypt(encMessage, privkey).decode()

#print("decrypted string: ", decMessage)