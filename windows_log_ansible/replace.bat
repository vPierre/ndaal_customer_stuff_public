@ECHO OFF &setlocal

FOR %%f IN (C:\ndaal\evtx_logs\*.evtx) DO (
set "oldname=%%~ff"
set "oldfname=%%~nf"
set "extension=%%~xf"
setlocal enabledelayedexpansion
set "filename=!oldfname:.=_!"
set "filename=!filename: =_!"
if not "!filename!"=="!oldfname!" RENAME "!oldname!" "!filename!!extension!"
endlocal
)

